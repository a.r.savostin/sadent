document.addEventListener('DOMContentLoaded', () => {
  // инициализация слайдера
  new ItcSimpleSlider('.itcss', {
    loop: true,
    autoplay: true,
    interval: 5000,
    swipe: true,
  });
});

let pursherBtns = document.querySelectorAll('.btn-grad')
let popUpWrap = document.querySelector('.pop_up_wrap');
let body = document.querySelector('body');
let itemIcon = document.querySelector('.contact_items_wrap');

for (let btns of pursherBtns) {
  btns.onclick = function () {
    popUpWrap.classList.add('ative_pop_up');
    itemIcon.classList.add('active_translate');
    body.classList.add('lock');
  };
}

popUpWrap.onclick = function () {
  popUpWrap.classList.remove('ative_pop_up');
  itemIcon.classList.remove('active_translate');
  body.classList.remove('lock');
};

let lastScroll = 0;
const defaultOffset = 30;
const fix_btn = document.querySelector('.fix_btn');

const scrollPosition = () => window.pageYOffset || document.documentElement.scrollTop;
const containHide = () => fix_btn.classList.contains('hide');

window.addEventListener('scroll', () => {
  if (scrollPosition() > lastScroll && !containHide() && scrollPosition() > defaultOffset) {
    //scroll down
    fix_btn.classList.add('hide');
  }
  else if (scrollPosition() < lastScroll && containHide()) {
    //scroll up
    fix_btn.classList.remove('hide');
  }

  lastScroll = scrollPosition();
})

let fixBnt = document.querySelector('.fix');
const time = 3000; //3 seconds
const timer = setInterval(function () {
  fixBnt.classList.toggle('active');
}, time);

// *** Анимация при скроле ***
const animateItems = document.querySelectorAll('.animate');

if (animateItems.length > 0) {

  window.addEventListener('scroll', animateOnScroll);

  function animateOnScroll() {

    for (let animateItem of animateItems) {
      const animateItemHeight = animateItem.offsetHeight;
      const animateItemOffset = offset(animateItem).top;
      const animateStart = 10;

      let animateItemPoint = window.innerHeight - animateItemHeight / animateStart;
      if (innerHeight > window.innerHeight) {
        animateItemPoint = window.innerHeight - window.innerHeight / animateStart;
      }

      if ((pageYOffset > animateItemOffset - animateItemPoint) && pageYOffset < (animateItemOffset + animateItemHeight)) {
        animateItem.classList.add('active', 'stop');
      } else {
        if (!animateItem.classList.contains('stop')) {
          animateItem.classList.remove('active');
        }
      }

    }

    function offset(el) {
      const rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
    }

  }

  setTimeout(() => {
    animateOnScroll();
  }, 400);
}

// auto copyripht data
function copyDataNow() {
  let DataNow = new Date();
  const copyriphtData = document.querySelector('.copyripht');
  copyriphtData.childNodes[1].innerText = DataNow.getFullYear();
}
copyDataNow();